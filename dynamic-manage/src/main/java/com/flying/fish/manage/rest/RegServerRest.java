package com.flying.fish.manage.rest;

import com.flying.fish.formwork.base.BaseRest;
import com.flying.fish.formwork.entity.RegServer;
import com.flying.fish.formwork.service.RegServerService;
import com.flying.fish.formwork.util.ApiResult;
import com.flying.fish.formwork.util.Constants;
import com.flying.fish.formwork.util.RouteConstants;
import com.flying.fish.manage.bean.RegServerReq;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @Description 客户端注册服务控制器类
 * @Author jianglong
 * @Date 2020/05/16
 * @Version V1.0
 */
@RestController
@RequestMapping("/regServer")
public class RegServerRest extends BaseRest {

    @Resource
    private RegServerService regServerService;

    @Resource
    private RedisTemplate redisTemplate;

    @RequestMapping(value = "/add", method = {RequestMethod.POST})
    public ApiResult add(@RequestBody RegServer regServer) {
        Assert.notNull(regServer, "未获取到对象");
        //默认禁止通行
        regServer.setStatus(Constants.NO);
        regServer.setCreateTime(new Date());
        this.validate(regServer);
        //验证注册服务是否重复
        RegServer qServer = new RegServer();
        qServer.setClientId(regServer.getClientId());
        qServer.setRouteId(regServer.getRouteId());
        long count = regServerService.count(qServer);
        Assert.isTrue(count <= 0, "客户端已注册该服务，请不要重复注册");
        //保存
        regServerService.save(regServer);
        this.setClientCacheVersion();
        return new ApiResult();
    }

    @RequestMapping(value = "/delete", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult delete(@RequestParam Long id) {
        Assert.notNull(id, "未获取到对象ID");
        Assert.isTrue(id>0, "ID值错误");
        regServerService.deleteById(id);
        this.setClientCacheVersion();
        return new ApiResult();
    }

    @RequestMapping(value = "/update", method = {RequestMethod.POST})
    public ApiResult update(@RequestBody RegServer regServer) {
        Assert.notNull(regServer, "未获取到对象");
        regServer.setUpdateTime(new Date());
        this.validate(regServer);
        regServerService.update(regServer);
        this.setClientCacheVersion();
        return new ApiResult();
    }

    @RequestMapping(value = "/findById", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult findById(@RequestParam Long id) {
        Assert.notNull(id, "未获取到对象ID");
        Assert.isTrue(id>0, "ID值错误");
        return new ApiResult(regServerService.findById(id));
    }

    @RequestMapping(value = "/serverPageList", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult serverPageList(@RequestBody RegServerReq regServerReq) {
        Assert.notNull(regServerReq, "未获取到对象");
        Assert.isTrue(StringUtils.isNotBlank(regServerReq.getClientId()), "未获取到对象查询ID");
        RegServer regServer = new RegServer();
        regServer.setClientId(regServerReq.getClientId());
        int currentPage = getCurrentPage(regServerReq.getCurrentPage());
        int pageSize = getPageSize(regServerReq.getPageSize());
        return new ApiResult(regServerService.serverPageList(regServer, currentPage, pageSize));
    }

    @RequestMapping(value = "/clientPageList", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult clientPageList(@RequestBody RegServerReq regServerReq) {
        Assert.notNull(regServerReq, "未获取到对象");
        Assert.isTrue(StringUtils.isNotBlank(regServerReq.getRouteId()), "未获取到对象查询ID");
        RegServer regServer = new RegServer();
        regServer.setRouteId(regServerReq.getRouteId());
        int currentPage = getCurrentPage(regServerReq.getCurrentPage());
        int pageSize = getPageSize(regServerReq.getPageSize());
        return new ApiResult(regServerService.clientPageList(regServer, currentPage, pageSize));
    }

    @RequestMapping(value = "/regClientList", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult regClientList(@RequestBody RegServerReq regServerReq) {
        Assert.notNull(regServerReq, "未获取到对象");
        Assert.isTrue(StringUtils.isNotBlank(regServerReq.getRouteId()), "未获取到对象查询ID");
        RegServer regServer = new RegServer();
        regServer.setRouteId(regServerReq.getRouteId());
        return new ApiResult(regServerService.regClientList(regServer));
    }

    @RequestMapping(value = "/start", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult start(@RequestParam Long id) {
        Assert.notNull(id, "未获取到对象ID");
        Assert.isTrue(id>0, "ID值错误");
        RegServer dbRegServer = regServerService.findById(id);
        dbRegServer.setStatus(Constants.YES);
        dbRegServer.setUpdateTime(new Date());
        regServerService.update(dbRegServer);
        this.setClientCacheVersion();
        return new ApiResult();
    }

    @RequestMapping(value = "/stop", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult stop(@RequestParam Long id) {
        Assert.notNull(id, "未获取到对象ID");
        Assert.isTrue(id>0, "ID值错误");
        RegServer dbRegServer = regServerService.findById(id);
        dbRegServer.setStatus(Constants.NO);
        dbRegServer.setUpdateTime(new Date());
        regServerService.update(dbRegServer);
        this.setClientCacheVersion();
        return new ApiResult();
    }

    @RequestMapping(value = "/stopClientAllRoute", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult stopClientAllRoute(@RequestParam String clientId) {
        Assert.isTrue(StringUtils.isNotBlank(clientId), "未获取到对象ID");
        regServerService.stopClientAllRoute(clientId);
        this.setClientCacheVersion();
        return new ApiResult();
    }

    @RequestMapping(value = "/startClientAllRoute", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult startClientAllRoute(@RequestParam String clientId) {
        Assert.isTrue(StringUtils.isNotBlank(clientId), "未获取到对象ID");
        regServerService.startClientAllRoute(clientId);
        this.setClientCacheVersion();
        return new ApiResult();
    }


    @RequestMapping(value = "/stopRouteAllClient", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult stopRouteAllClient(@RequestParam String routeId) {
        Assert.isTrue(StringUtils.isNotBlank(routeId), "未获取到对象ID");
        regServerService.stopRouteAllClient(routeId);
        this.setClientCacheVersion();
        return new ApiResult();
    }

    @RequestMapping(value = "/startRouteAllClient", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult startRouteAllClient(@RequestParam String routeId) {
        Assert.isTrue(StringUtils.isNotBlank(routeId), "未获取到对象ID");
        regServerService.startRouteAllClient(routeId);
        this.setClientCacheVersion();
        return new ApiResult();
    }

    @RequestMapping(value = "/notRegServerPageList", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult notRegServerPageList(@RequestBody RegServerReq regServerReq) {
        Assert.notNull(regServerReq, "未获取到对象");
        Assert.isTrue(StringUtils.isNotBlank(regServerReq.getClientId()), "未获取到客户端ID");
        int currentPage = getCurrentPage(regServerReq.getCurrentPage());
        int pageSize = getPageSize(regServerReq.getPageSize());
        return new ApiResult(regServerService.notRegServerPageList(regServerReq, currentPage, pageSize));
    }

    @RequestMapping(value = "/notRegClientPageList", method = {RequestMethod.GET, RequestMethod.POST})
    public ApiResult notRegClientPageList(@RequestBody RegServerReq regServerReq) {
        Assert.notNull(regServerReq, "未获取到对象");
        Assert.isTrue(StringUtils.isNotBlank(regServerReq.getRouteId()), "未获取路由服务ID");
        int currentPage = getCurrentPage(regServerReq.getCurrentPage());
        int pageSize = getPageSize(regServerReq.getPageSize());
        return new ApiResult(regServerService.notRegClientPageList(regServerReq, currentPage, pageSize));
    }

    /**
     * 对客户端数据进行变更后，设置redis中缓存的版本号
     */
    private void setClientCacheVersion(){
        redisTemplate.opsForHash().put(RouteConstants.SYNC_VERSION_KEY, RouteConstants.CLIENT_ID, String.valueOf(System.currentTimeMillis()));
    }

}
