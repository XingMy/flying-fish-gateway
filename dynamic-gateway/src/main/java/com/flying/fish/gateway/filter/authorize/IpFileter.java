package com.flying.fish.gateway.filter.authorize;

import com.flying.fish.formwork.util.NetworkIpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.server.reactive.ServerHttpRequest;

/**
 * @Description 验证当前IP是否可访问（只限定指定IP访问）
 * @Author jianglong
 * @Date 2020/05/25
 * @Version V1.0
 */
@Slf4j
public class IpFileter extends FilterHandler {

    public IpFileter(FilterHandler handler){
        this.handler = handler;
    }

    @Override
    public void handleRequest(ServerHttpRequest request){
        log.info("处理网关路由请求{},执行ip过滤 ", route.getId());
        if (route.getFilterAuthorizeName().contains("ip")){
            String ip = NetworkIpUtils.getIpAddress(request);
            if (route.getAccessIp()!=null && route.getAccessIp().contains(ip)){
            }else {
                throw new IllegalStateException("执行ip过滤,自定义ip验证不通过 请求源="+ip+",自定义目标=" + route.getAccessIp());
            }
        }
    }

}
